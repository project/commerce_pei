### Summary

This module provides a payment method using [Pei](https://pei.is).

### Installation

Available via Composer: `composer require drupal/commerce_pei`

### Configuration

1. Visit `admin/commerce/config/payment-gateways`
2. Add a new payment gateway and select `Pei` plugin.
3. Adjust settings and enter credentials provided by Pei.

Test mode credentials are publicly available:

- Client ID: `democlient`
- Secret: `demosecret`
- Merchant ID: `4`

### Sponsors

- [Eldum rétt](https://eldumrett.is)

### Contact

Developed and maintained by:

- [1xINTERNET](https://1xinternet.de)
- [Cambrico](https://cambrico.net)
- and more...
