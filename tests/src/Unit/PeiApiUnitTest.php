<?php

namespace Drupal\Tests\commerce_pei\Unit;

use Drupal\commerce_pei\Api\PeiApi;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * @coversDefaultClass \Drupal\commerce_pei\Api\PeiApi
 * @group commerce_pei
 */
class PeiApiUnitTest extends UnitTestCase {

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $client;

  /**
   * The mock handler for responses.
   *
   * @var \GuzzleHttp\Handler\MockHandler
   */
  protected $mockHandler;

  /**
   * The mock logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @var array
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->mockHandler = new MockHandler();
    $handler = HandlerStack::create($this->mockHandler);
    $this->client = new HttpClient(['handler' => $handler]);
    $this->logger = $this->getMockBuilder(LoggerChannelInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->config = $this->getMockBuilder(ConfigFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
  }

  /**
   * @covers ::__construct.
   */
  public function testTestEnvironmentIsValid() {
    $api = new PeiApi($this->config, $this->client, $this->logger);
    $this->assertTrue(TRUE);
  }

  /**
   * @covers ::authorizer->validate.
   */
  public function testAuthenticateFailsWithAuthorizationDenied() {
    $this->mockHandler->append(new Response(401, [], '{"error":"Authorization has been denied for this request."}'));

    $api = new PeiApi($this->config, $this->client, $this->logger);
    $authenticated = $api->authorizer->validate('test', 'user', 'password');
    $this->assertFalse($authenticated);
  }

  /**
   * @covers ::authorizer->validate.
   */
  public function testAuthenticateSuccess() {
    $this->mockHandler->append(new Response(200, [], '{"access_token":"my-token","expires_in": 3600,"token_type": "Bearer"}'));

    $api = new PeiApi($this->config, $this->client, $this->logger);
    $is_valid = $api->authorizer->validate('test', 'user', 'password');
    $this->assertTrue($is_valid);
  }

  /**
   * @covers ::purchaseAccess->hasAccess
   */
  public function testCheckAccessToBuyerSuccess() {
    $this->mockHandler->append(new Response(200, [], 'true'));

    $api = new PeiApi($this->config, $this->client, $this->logger);
    $has_access = $api->purchaseAccess->hasAccess('ssn_success');
    $this->assertTrue($has_access);
  }

  /**
   * @covers ::purchaseAccess->hasAccess
   */
  public function testCheckAccessToBuyerFailure() {
    $this->mockHandler->append(new Response(400, [], 'false'));

    $api = new PeiApi($this->config, $this->client, $this->logger);
    $has_access = $api->purchaseAccess->hasAccess('ssn_failure');
    $this->assertFalse($has_access);
  }

  /**
   * @covers ::purchaseAccess->requestAccess
   */
  public function testRequestAccessToBuyerSuccess() {
    $this->mockHandler->append(new Response(200, [], 'true'));

    $api = new PeiApi($this->config, $this->client, $this->logger);
    $request_status = $api->purchaseAccess->requestAccess('ssn_success', '1111111');
    $this->assertFalse($request_status, NULL);
  }

  /**
   * @covers ::purchaseAccess->requestAccess
   */
  public function testRequestAccessToBuyerFailure() {
    $this->mockHandler->append(new Response(400, [], 'false'));

    $api = new PeiApi($this->config, $this->client, $this->logger);
    $request_status = $api->purchaseAccess->requestAccess('ssn_failure', '0000000');
    $this->assertFalse(is_string($request_status));
  }

}
