<?php

namespace Drupal\commerce_pei\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the Pei payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "pei",
 *   label = @Translation("Pei"),
 *   create_label = @Translation("New pei payment method"),
 * )
 */
class PeiPaymentMethodType extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Pei with ISSN @issn', ['@issn' => $payment_method->issn->value]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['telephone'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Telephone number'))
      ->setDescription($this->t('Payers phone number for verification.'))
      ->setRequired(TRUE);

    $fields['issn'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('The payers ISSN'))
      ->setDescription($this->t('The payers international standard serial number (ISSN).'))
      ->setRequired(TRUE);

    $fields['pincode'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Verification code'))
      ->setDescription($this->t('SMS verification code (PIN).'))
      ->setRequired(TRUE);

    return $fields;
  }

}
