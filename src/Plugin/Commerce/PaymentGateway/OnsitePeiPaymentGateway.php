<?php

namespace Drupal\commerce_pei\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsUpdatingStoredPaymentMethodsInterface;
use Drupal\commerce_pei\Api\PeiApi;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Pei payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "pei",
 *   label = "Pei",
 *   display_label = "Pei",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_pei\PluginForm\PeiPaymentMethodAddForm",
 *     "edit-payment-method" = "Drupal\commerce_pei\PluginForm\PaymentMethodEditForm"
 *   },
 *   payment_method_types = {"pei"},
 * )
 */
class OnsitePeiPaymentGateway extends OnsitePaymentGatewayBase implements OnsitePaymentGatewayInterface, SupportsUpdatingStoredPaymentMethodsInterface {

  use MessengerTrait;

  /**
   * Pei API client.
   *
   * @var \Drupal\commerce_pei\Api\PeiApi
   */
  protected PeiApi $pei;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a new OnsitePeiPaymentGateway instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The base field definitions for a content entity type.
   * @param \Drupal\commerce_pei\Api\PeiApi $pei
   *   Pei API client service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, MinorUnitsConverterInterface $minor_units_converter, EntityFieldManagerInterface $entityFieldManager, PeiApi $pei) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);
    $this->entityFieldManager = $entityFieldManager;
    $this->pei = $pei;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('entity_field.manager'),
      $container->get('pei')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'merchant_id' => '',
        'user_id' => '',
        'password' => '',
        'copy_profile_fields' => 0,
        'mapping_issn' => '',
        'mapping_telephone' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $client_id = $this->configuration['user_id'];
    $secret = $this->configuration['password'];

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('The merchant ID given by Pei for this store'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];
    $form['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User ID'),
      '#description' => $this->t('The user ID given by Pei for this store'),
      '#default_value' => $client_id,
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The password given by Pei for this store'),
      '#default_value' => $secret,
      '#required' => TRUE,
    ];
    $form['copy_profile_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Copy billing profile fields into payment method form as default values'),
      '#description' => $this->t('Allows to pre-fill the form with customer data'),
      '#default_value' => $this->configuration['copy_profile_fields'],
      '#return_value' => 1,
    ];
    // Get the fields that profile of type customer has for mapping.
    $fields = $this->entityFieldManager->getFieldDefinitions('profile', 'customer');
    $profile_fields = [];
    $profile_fields[''] = $this->t('-- Choose profile field --');
    foreach ($fields as $fieldId => $field) {
      $profile_fields[$fieldId] = $field->getLabel();
    }
    // Add the setting for ISSN mapping.
    $form['mapping_issn'] = [
      '#type' => 'select',
      '#default_value' => $this->configuration['mapping_issn'],
      '#title' => $this->t('ISSN field from profile'),
      '#description' => $this->t('Select the field from billing that represents the ISSN'),
      '#options' => $profile_fields,
      '#states' => [
        'visible' => [
          ':input[name="configuration[pei][copy_profile_fields]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    // Add the setting for Telephone mapping.
    $form['mapping_telephone'] = [
      '#type' => 'select',
      '#default_value' => $this->configuration['mapping_telephone'],
      '#title' => $this->t('Telephone field from profile'),
      '#description' => $this->t('Select the field from billing that represents the telephone number'),
      '#options' => $profile_fields,
      '#states' => [
        'visible' => [
          ':input[name="configuration[pei][copy_profile_fields]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $form_values = ($form_state->getValues())['configuration']['pei'];
    $success = $this->pei->authorizer->validate($form_values['mode'], $form_values['user_id'], $form_values['password']); //testAuthorization($form_values['mode'], $form_values['user_id'], $form_values['password']);
    if (!$success) {
      $form_state->setError(
        $form['user_id'],
        $this->t('Pei authorization token could not be obtained with the given credentials (in "@mode" mode). Make sure credentials and mode are correct. See logs for more details.', ['@mode' => $form_values['mode']])
      );
      $form_state->setError($form['password']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['user_id'] = $values['user_id'];
      $this->configuration['password'] = $values['password'];
      $this->configuration['copy_profile_fields'] = $values['copy_profile_fields'];
      $this->configuration['mapping_issn'] = $values['mapping_issn'];
      $this->configuration['mapping_telephone'] = $values['mapping_telephone'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // Buyer access/authorization is performed in `createPaymentMethod` method.
    if (!$capture) {
      return;
    }
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $remote_id = $this->pei->orders->submit($payment);

    $payment->setRemoteId($remote_id);
    $payment->setState('completed');
    $payment->setAuthorizedTime($this->time->getRequestTime());
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    // The expected keys are payment gateway specific and usually match
    // the PaymentMethodAddForm form elements. They are expected to be valid.
    $required_keys = [
      'telephone',
      'issn',
      'pincode',
    ];
    $missing = [];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        $missing[] = $required_key;
      }
    }
    if (!empty($missing)) {
      $keys = implode(', ', $missing);
      throw new \InvalidArgumentException("Missing payment details key(s): '$keys'.");
    }

    $issn = $payment_details['issn'];
    $this->pei->purchaseAccess->hasAccess($issn);

    // Non-reusable payment methods usually have an expiration timestamp.
    $payment_method->telephone = $payment_details['telephone'];
    $payment_method->issn = $payment_details['issn'];
    $payment_method->pincode = $payment_details['pincode'];
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->save();
  }

}
