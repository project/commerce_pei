<?php

namespace Drupal\commerce_pei\Api;

use Drupal\commerce_payment\Exception\AuthenticationException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Psr\Http\Message\ResponseInterface;

class PeiApiException extends PaymentGatewayException {

  /**
   * For translating response messages.
   */
  use ResponseTrait, StringTranslationTrait;

  /**
   * List of all known Pei error codes and associated message.
   * @internal
   */
  private const RESPONSE_ERROR_CODES = [
    10001 => 'Order exceeds merchant\'s financer allowance',  // MerchantAllowance
    10002 => 'Ssn was not found in the icelandic registry',  // SsnNotInRegistry
    10003 => 'Pei credit rating insufficient',  // CreditRating
    10004 => 'Order exceeds buyer allowance',  // BuyerAllowance
    10005 => 'Failed to charge payment card',  // ChargePaymentCard
    10006 => 'Mobile number not trusted for this ssn',  // MobileNumberNotTrusted
    10007 => 'The provided pin was not correct',  // InvalidPin
    10008 => 'Order has already been charged',  // OrderAlreadyCharged
    10009 => 'Order is not configured for postback',  // OrderNoPostbackUrl
    10010 => 'Failed to send postback',  // OrderPostbackFailed
    10011 => 'Merchant payment options settings give no available options for this order\'s low amount',  // OrderHasNoPaymentOptions
    10012 => 'Mobile number was invalid or the text message was null',  // FailedToSendTextMessage
    10013 => 'Authorization code for the buyer was not found',  // AuthorizationCode
    10014 => 'Order has expired',  // OrderExpired
    10015 => 'Unable to connect to Creditinfo services',  // CreditInfo
    10016 => 'Unregistered buyers cannot distribute orders',  // Distribute
    10017 => 'Unregistered buyers cannot postpone orders',  // ExtraMonths
    10018 => 'Pei credit rating insufficient, with a specified reason',  // PublicCreditRatingMessage
    10019 => 'Payment plan has not been set for the order',  // PaymentPlanNotSet
    10020 => 'Order has been cancelled',  // OrderCancelled
    10021 => 'Illegal payment plan as been set for the order',  // PaymentPlanIllegal
    10022 => 'The order has an illegal calculated APR',  // AprIllegal
    10023 => 'Insufficient buyer information to charge this order',  // BuyerInformation
    10024 => 'Merchant settings does not allow the number of installments for this payment method',  // MerchantInstallments
    10025 => 'Payment plan mismatch between extraMonths and numberOfInstallments',  // PaymentPlanMismatch
    10026 => 'Various error messages relating to the merchant\'s settings',  // MerchantSettings
    10027 => 'Merchant does not allow unregistered buyers',  // MerchantDoesNotAllowUnregisteredBuyers
    10028 => 'Order is not confirmed',  // OrderIsNotConfirmed
    10029 => 'Order is already settled',  // OrderIsSettled
    10030 => 'Order has already been back charged',  // OrderBackCharged
    10031 => 'Order has not been charged',  // OrderNotCharged
    10032 => 'Authentication failed',  // Authentication
    10033 => 'Order not found',  // OrderNotFound
    10034 => 'Card number is missing',  // MissingCardNumber
    10035 => 'Card number is of incorrect length',  // IncorrectLengthOfCardNumber
    10036 => 'Card is invalid',  // InvalidCard
    10037 => 'Card expiration date is missing',  // CardExpirationDateIsMissing
    10038 => 'Card expiration date is incorrect',  // CardExpirationDateIsIncorrect
    10039 => 'Card expiration date has passed',  // CardIsExpired
    10040 => 'Card transaction was declined',  // CardDeclined
    10041 => 'Fraud was suspected',  // FraudSuspected
    10042 => 'Card is presumed lost',  // CardIsPresumedLost
    10043 => 'Card is presumed stolen',  // CardIsPresumedStolen
    10044 => 'Card is presumed stolen',  // CardIssuerWasUnreachable
    10045 => 'User buyer has not agreed to terms',  // UserBuyerHasNotAgreedToTerms
    10046 => 'Distribution type of order is not lump sum',  // DistributionTypeIsNotLumpSum
    10047 => 'Modification of installment is not allowed',  // InstallmentModificationException
    10048 => 'Order is not a purchase, e.g. it is a subscription or a purchase request',  // OrderTypeIsNotPurchase
    10049 => 'Buyer is not allowed to be a company for this operation',  // BuyerIsCompany
    10050 => 'Cannot complete the operation for this order because it has not been factored yet',  // OrderIsPendingFactoring
    10051 => 'The error that occurred is unknown',  // UnknownError
    10052 => 'The Claim Identity has not been registered',  // ClaimIdentityNotRegistered
    10053 => 'Exponent sent to acquirer was not expected by Issuer',  // IssuerExponentError
    10054 => 'The action is not possible until some time after the authorization has been cleared.',  // PaymentAgeLimit
    10055 => 'The payment retry was not allowed', // RetryNotAllowed
  ];

  /**
   * @param \GuzzleHttp\Exception\GuzzleException $e
   *
   * @return \Drupal\commerce_payment\Exception\PaymentGatewayException
   * @api
   */
  public static function handle(LoggerChannelInterface $logger, GuzzleException $e): PaymentGatewayException {
    self::log($logger, $e);
    $message = $e->getMessage();
    $code = $e->getCode();
    if (!$e->hasResponse()) {
      return new PaymentGatewayException($message, $code, $e);
    }
    $response = $e->getResponse();
    $response_message = self::message($response);
    if (!empty($response_message)) {
      $message = $response_message;
    }
    if ($e instanceof ServerException) {
      return new InvalidResponseException($message, $code, $e);
    }
    return match ($code) {
      400, 404, 405 => new InvalidRequestException($message, $code, $e),
      401 => new AuthenticationException($message, $code, $e),
      403 => new DeclineException($message, $code, $e),
      default => new PaymentGatewayException($message, $code, $e),
    };
  }

  /**
   * @param ResponseInterface $response
   *
   * @return string
   * @api
   */
  private static function message(ResponseInterface $response): string {
    $body = self::parseResponseBody($response);
    if (isset($body['code']) && array_key_exists(intval($body['code']), self::RESPONSE_ERROR_CODES)) {
      return (string)t(self::RESPONSE_ERROR_CODES[intval($body['code'])]);
    }
    return $body ?? '';
  }

  /**
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   * @param RequestException $e
   *
   * @return void
   * @internal
   */
  private static function log(LoggerChannelInterface $logger, GuzzleException $e): void {
    $details = [
      '@statusCode' => $e->getCode(),
      '@message' => $e->getMessage(),
      '@trace' => $e->getTraceAsString(),
    ];
    if (!$e->hasResponse()) {
      $logger->error(
        'Pei API error occurred, no response received. Message: @message (@trace).',
        $details
      );
    }
    else {
      $response = $e->getResponse();
      $details['@reasonPhrase'] = $response->getReasonPhrase();
      $logger->error(
        'Pei API exception occurred. "@statusCode @reasonPhrase" status code received. Message: @message (@trace).',
        $details,
      );
    }
  }

}