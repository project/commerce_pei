<?php

namespace Drupal\commerce_pei\Api\Resources;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\InvalidResponseException;

class Orders extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  protected const TYPE = 'orders';

  /**
   * Purchase an order for a buyer (submits a remote order).
   *
   * @param PaymentInterface $payment
   *
   * @return mixed
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @link https://api.pei.is/docs/ui/index#!/Orders/OrdersV1_PayOrder
   *
   * @api
   */
  public function submit(PaymentInterface $payment): string {
    $order = $payment->getOrder();
    $buyer = $order->getBillingProfile();
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $buyer->get('address')->first();
    $payment_method = $payment->getPaymentMethod();
    $order_items = [];
    foreach ($order->getItems() as $item) {
      $purchasedEntity = $item->getPurchasedEntity();
      $order_items[] = [
        'code' => $purchasedEntity->getSku(),
        'name' => $item->getTitle(),
        'quantity' => $item->getQuantity(),
        'unit' => 'units',
        'unitPrice' => $item->getUnitPrice()->getNumber(),
        'amount' => $item->getAdjustedTotalPrice()->getNumber(),
      ];
    }

    $data = [
      'merchantId' => $this->api->merchantId,
      'buyer' => [
        'name' => sprintf('%s %s', trim($address->getGivenName()), trim($address->getFamilyName())),
        'ssn' => $payment_method->get('issn')->value,
        'email' => $order->getEmail(),
        'mobileNumber' => $payment_method->get('telephone')->value,
      ],
      'amount' => $payment->getAmount()->getNumber(),
      'reference' => $order->id(),
      'successReturnUrl' => '',
      'cancelReturnUrl' => '',
      'postbackUrl' => '',
      'items' => $order_items,
    ];

    $endpoint = $this->endpoint('pay');
    $body = $this->api->request('POST', $endpoint, $data);
    if (empty($body['orderId'])) {
      throw new InvalidResponseException('Response body either does not contain property "orderId" or its value is empty.');
    }
    return $body['orderId'];
  }

}