<?php

namespace Drupal\commerce_pei\Api\Resources;

class PurchaseAccess extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  protected const TYPE = 'purchaseaccess';

  /**
   * Check if merchant has access to purchase orders for a buyer.
   *
   * @param string $buyer_ssn
   *   Buyer's SSN.
   *
   * @return bool
   * @api
   * @link https://api.pei.is/docs/ui/index#!/PurchaseAccess/PurchaseAccessV1_HasAccess
   *
   */
  public function hasAccess(string $buyer_ssn): bool {
    $query = [
      'merchantId' => $this->api->merchantId,
      'buyerSsn' => $buyer_ssn,
    ];
    $endpoint = $this->endpoint();
    /** @var bool $has_access */
    $has_access = $this->api->request('GET', $endpoint, NULL, $query);
    return $has_access === TRUE;
  }

  /**
   * Confirm merchant access to purchase orders for the buyer with a pin from
   * the buyer.
   *
   * @param string $buyer_ssn
   *   Buyer's SSN.
   * @param string $pin
   *   Pin code (TAN).
   *
   * @return
   * @link https://api.pei.is/docs/ui/index#!/PurchaseAccess/PurchaseAccessV1_ConfirmAccess
   *
   * @api
   */
  public function confirmAccess(string $buyer_ssn, string $pin) {
    $data = [
      'merchantId' => $this->api->merchantId,
      'buyerSsn' => $buyer_ssn,
      'pin' => $pin,
    ];
    $endpoint = $this->endpoint();
    $body = $this->api->request('POST', $endpoint, $data);
    return $body;
  }

  /**
   * Request access to purchase orders for a buyer by sending a pin to the
   * buyer.
   *
   * @param string $buyer_ssn
   *   Buyer's SSN.
   * @param string $mobile_number
   *   Buyer's mobile phone number.
   *
   * @return string|NULL
   * @api
   * @link https://api.pei.is/docs/ui/index#!/PurchaseAccess/PurchaseAccessV1_RequestAccess
   *
   */
  public function requestAccess(string $buyer_ssn, string $mobile_number): string|null {
    $data = [
      'merchantId' => $this->api->merchantId,
      'buyerSsn' => $buyer_ssn,
      'mobileNumber' => $mobile_number,
    ];
    $endpoint = $this->endpoint('request');
    $body = $this->api->request('POST', $endpoint, $data);
    return $body; // NULL OR 'This merchant can already purchase orders for this buyer.'
  }

}