<?php

namespace Drupal\commerce_pei\Api\Resources;

use Drupal\commerce_pei\Api\PeiApi;

abstract class ResourceBase {

  /**
   * Resource endpoint type.
   *
   * @var string
   * @internal
   */
  protected const TYPE = '';

  /**
   * @var PeiApi
   */
  protected readonly PeiApi $api;

  /**
   * Resource constructor
   *
   * @param PeiApi $api
   *
   */
  public function __construct(PeiApi $api) {
    $this->api = $api;
  }

  /**
   * @param string $parameter
   *
   * @return string
   * @internal
   */
  protected function endpoint(string $parameter = ''): string {
    return 'api/' . static::TYPE . '/' . $parameter;
  }

}