<?php

namespace Drupal\commerce_pei\Api;

use Drupal\commerce_payment\Exception\AuthenticationException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\RequestInterface;

class PeiApiAuthorization {

  use ResponseTrait;

  /**
   * Authorization base URL for TEST environment mode.
   */
  private const BASE_URL_TEST = 'https://authstaging.pei.is/core/connect/token';

  /**
   * Authorization base URL for LIVE environment mode.
   */
  private const BASE_URL_LIVE = 'https://auth.pei.is/core/connect/token';

  /**
   * @var \GuzzleHttp\Client
   */
  private readonly Client $client;

  /**
   * The logger channel.
   *
   * @var LoggerChannelInterface
   */
  protected readonly LoggerChannelInterface $logger;

  /**
   * @var \GuzzleHttp\Psr7\Request
   */
  private readonly Request $request;

  /**
   * @var array
   */
  private readonly array $requestOptions;

  /**
   * @var string|null
   */
  private ?string $accessToken;

  /**
   * @param Client $client
   * @param LoggerChannelInterface $logger
   * @param string $environment
   * @param string $client_id
   * @param string $secret
   */
  public function __construct(Client $client, LoggerChannelInterface $logger, string $environment, string $client_id, string $secret) {
    // TODO: initialize Client
    $this->client = $client;
    $this->logger = $logger;
    $this->accessToken = NULL;
    $this->request = $this->prepareAccessRequest($environment);
    $this->requestOptions = self::prepareRequestOptions($client_id, $secret);
  }

  /**
   * Test whether the given credentials and environment yield a valid access
   * token.
   *
   * @param string $environment
   * @param string $client_id
   * @param string $secret
   *
   * @return bool
   * @api
   */
  public function validate(string $environment, string $client_id, string $secret): bool {
    try {
      $request = $this->prepareAccessRequest($environment);
      $request_options = self::prepareRequestOptions($client_id, $secret);
      $access_token = $this->sendAccessTokenRequest($request, $request_options);
      return $this->isValid($access_token);
    } catch (PaymentGatewayException $e) {
      return FALSE;
    }
  }

  /**
   * Fetches and returns Pei authorization if it hasn't been fetched already or
   * if expired.
   *
   * @return string
   * @api
   */
  public function getToken(): string {
    if (!$this->isValid($this->accessToken) /* OR IF EXPIRED */) {
      $this->accessToken = $this->sendAccessTokenRequest($this->request, $this->requestOptions);
    }
    return $this->accessToken;
  }

  /**
   * @param string $client_id
   * @param string $secret
   *
   * @return array
   * @internal
   */
  private static function prepareRequestOptions(string $client_id, string $secret): array {
    return [
      RequestOptions::AUTH => [
        $client_id,
        $secret,
      ],
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
      RequestOptions::FORM_PARAMS => [
        'grant_type' => 'client_credentials',
        'scope' => 'externalapi',
      ],
      RequestOptions::HTTP_ERRORS => TRUE,
    ];
  }

  /**
   * @param $access_token
   *
   * @return bool
   * @internal
   */
  private function isValid($access_token): bool {
    return !is_null($access_token);
  }

  /**
   * @param RequestInterface $request
   * @param array $requestOptions
   *
   * @return string Bearer access token
   *
   * @internal
   */
  private function sendAccessTokenRequest(RequestInterface $request, array $requestOptions): string {
    try {
      $response = $this->client->send($request, $requestOptions);
      $body = self::parseResponseBody($response);
      return $body['access_token'];
    } catch (GuzzleException $e) {
      throw new AuthenticationException('Could not acquire a Pei access token.', $e->getCode(), $e);
    }
    // It'd not be ill-advised to take the expires_in property in to account when deciding 
    /*{
      "access_token": "foobar",
      "expires_in": 3600,
      "token_type": "Bearer"
    }*/
  }

  /**
   * @param string $environment
   *
   * @return \GuzzleHttp\Psr7\Request
   * @internal
   */
  private function prepareAccessRequest(string $environment): Request {
    $url = match ($environment) {
      'live' => self::BASE_URL_LIVE,
      'test' => self::BASE_URL_TEST,
      default => throw new InvalidRequestException(
        "Invalid Pei payment gateway mode: \"$environment\". Value must be \"live\" or \"test\"."
      )
    };
    return new Request('POST', $url);
  }

}