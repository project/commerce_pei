<?php

namespace Drupal\commerce_pei\Api;

use Drupal\Component\Serialization\Json;
use Psr\Http\Message\ResponseInterface;

trait ResponseTrait {

  /**
   * @param \Psr\Http\Message\ResponseInterface $response
   *
   * @return mixed
   * @internal
   */
  protected static function parseResponseBody(ResponseInterface $response): mixed {
    $next = Json::decode($response->getBody());
    $prev = $next;
    while (TRUE) {
      if (is_null($next)) {
        return $prev;
      }
      elseif (!is_string($next)) {
        return $next;
      }
      $prev = $next;
      $next = Json::decode($next);
    }
  }

}