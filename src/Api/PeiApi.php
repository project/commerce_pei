<?php

namespace Drupal\commerce_pei\Api;

use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_pei\Api\Resources\Orders;
use Drupal\commerce_pei\Api\Resources\PurchaseAccess;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

final class PeiApi {

  use ResponseTrait;

  /**
   * API client base url for LIVE mode.
   */
  private const BASE_URL_LIVE = 'https://api.pei.is';

  /**
   * API client base url for TEST mode.
   */
  private const BASE_URL_TEST = 'https://externalapistaging.pei.is';

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected readonly ClientInterface $client;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected readonly LoggerChannelInterface $logger;

  /**
   * @var \Drupal\commerce_pei\Api\PeiApiAuthorization
   */
  public readonly PeiApiAuthorization $authorizer;

  /**
   * Pei Merchant ID, set in Payment Gateway config.
   *
   * @var string
   */
  public readonly string $merchantId;

  /**
   * Resource: Orders.
   *
   * @var Orders
   * @api
   */
  public readonly Orders $orders;

  /**
   * Resource: Purchase access.
   *
   * @var PurchaseAccess
   * @api
   */
  public readonly PurchaseAccess $purchaseAccess;

  /**
   * @var string
   */
  protected readonly string $baseUrl;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \GuzzleHttp\ClientInterface $http_client
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerChannelInterface $logger_channel) {
    $this->client = $http_client;
    $this->logger = $logger_channel;

    $config = $config_factory->get('commerce_payment.commerce_payment_gateway.pei')
      ->get('configuration');

    $this->authorizer = new PeiApiAuthorization(
      $this->client,
      $this->logger,
      $config['mode'],
      $config['user_id'],
      $config['password']
    );

    $this->merchantId = $config['merchant_id'];

    $this->baseUrl = match ($config['mode']) {
      'live' => self::BASE_URL_LIVE,
      'test' => self::BASE_URL_TEST,
      default => throw new InvalidRequestException(
        "Invalid Pei payment gateway mode: \"{$config['mode']}\". Value must be \"live\" or \"test\"."
      )
    };

    $this->orders = new Orders($this);
    $this->purchaseAccess = new PurchaseAccess($this);
  }

  /**
   * @param string $method
   * @param string $path
   * @param array|NULL $data
   * @param array|NULL $query
   *
   * @return mixed
   * @api
   */
  public function request(string $method, string $path, array $data = NULL, array $query = NULL): mixed {
    $options = [
      RequestOptions::HTTP_ERRORS => TRUE,
      RequestOptions::ALLOW_REDIRECTS => FALSE,
    ];
    // TODO: Test bearer vs. Bearer
    $options[RequestOptions::HEADERS] = [
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
      'Authorization' => "bearer {$this->authorizer->getToken()}",
    ];
    if (!is_null($data)) {
      $options[RequestOptions::JSON] = $data;
    }
    if (!is_null($query)) {
      $options[RequestOptions::QUERY] = $query;
    }

    $url = $this->baseUrl . '/' . $path;
    try {
      $response = match ($method) {
        'GET' => $this->client->get($url, $options),
        'POST' => $this->client->post($url, $options),
        default => throw new InvalidRequestException("Invalid HTTP request method '$method'."),
      };
      return self::parseResponseBody($response);
    } catch (RequestException $e) {
      throw PeiApiException::handle($this->logger, $e);
    }
  }

}