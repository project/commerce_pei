<?php

namespace Drupal\commerce_pei\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentMethodEditForm as CommercePaymentMethodEditForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * The payment method edit form plugin.
 */
class PaymentMethodEditForm extends CommercePaymentMethodEditForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $payment_method_label = $this->entity->label();
    $form['payment_details']['heading'] = [
      '#markup' => '<h2>' . $this->t('Edit @label', ['@label' => $payment_method_label]) . '</h2>',
      '#weight' => -100,
    ];
    $form['billing_information']['#access'] = FALSE;
    return $form;
  }

}
